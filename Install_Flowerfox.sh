#!/bin/bash

#linux edition
#v0.1a

echo "Welcome to the Flowerfox Installer (for Firefox 89+)"
echo
echo "It is recommended that you use a separate installation of Firefox to use with Flowerfox."
echo
echo "You can get a distribution-agnostic build of Firefox at 'https://www.mozilla.org/en-US/firefox/new/'."
echo
echo "Otherwise, you might find Firefox at '/usr/lib/firefox'."
echo
echo "Enter path to Firefox application directory [do not end with '/']:"
echo -n "FIREFOX PATH: "
read installation_path
cd $installation_path




# -------- Prepare Firefox: -------- #

#https://github.com/xiaoxiaoflood/firefox-scripts
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/fx-folder.zip
unzip -o fx-folder.zip
rm fx-folder.zip



# -------- Create or use profile -------- #

echo "Enter path for new profile (do not use an existing profile):"
echo -n "PROFILE PATH: "
read profile_path

mkdir $profile_path
cd $profile_path



# -------- Install various .uc.js addons -------- #

mkdir chrome
cd chrome

#https://github.com/xiaoxiaoflood/firefox-scripts
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/utils.zip
unzip -o utils.zip
rm utils.zip

#https://github.com/xiaoxiaoflood/firefox-scripts
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/BeQuiet.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/autoPlainTextLinks.framescript.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/autoPlainTextLinks.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/autoPlainTextLinks.zip
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/enterSelects.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/extensionOptionsMenu.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/extensionsUpdateChecker.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/keepMenuOnMiddleClick.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/masterPasswordPlus.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/minMaxCloseButton.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/mouseGestures.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/multifoxContainer.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/newtab-aboutconfig.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/openInUnloadedTab.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/privateTab.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/rebuild_userChrome.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/redirector.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/scroll1pxWheel.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/scrollUpFolder.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/status-bar.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/styloaix.uc.js
wget https://raw.githubusercontent.com/xiaoxiaoflood/firefox-scripts/master/chrome/styloaix.zip
unzip -o styloaix.zip
unzip -o autoPlainTextLinks

#https://github.com/Aris-t2/CustomJSforFx
wget https://raw.githubusercontent.com/Aris-t2/CustomJSforFx/master/scripts/space_and_separator_restorer.uc.js
wget https://raw.githubusercontent.com/Aris-t2/CustomJSforFx/master/scripts/search_engine_icon_in_searchbar.uc.js



# -------- Install Flowerfox Dark Theme -------- #

wget https://gitlab.com/flowerfox1/flowerfox/-/raw/main/Flowerfox%20Dark%20Theme/userChrome.css



# -------- Set Default Prefs -------- #

cd $profile_path
rm prefs.js
wget https://gitlab.com/flowerfox1/flowerfox/-/raw/main/Default%20Prefs/prefs.js



# -------- Install icons -------- #

mkdir ~/.local/share/icons #in case it doesn't exist
cd ~/.local/share/icons
mkdir ~/.local/share/icons/hicolor #in case it doesn't exist
cd ~/.local/share/icons/hicolor

wget https://gitlab.com/flowerfox1/flowerfox/-/raw/main/Flowerfox%20Icon/flowerfox_hicolor.zip
unzip -o flowerfox_hicolor.zip
rm flowerfox_hicolor.zip



# -------- Create .desktop -------- #

mkdir ~/.local/share/applications #in case it doesn't exist
cd ~/.local/share/applications
rm flowerfox.desktop

cat > ~/.local/share/applications/flowerfox.desktop <<EOL
[Desktop Entry]
Name=Flowerfox
Exec=$installation_path/firefox --profile "$profile_path" %u
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=flowerfox
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
EOL



# -------- End -------- #

echo
echo Successfully installed Flowerfox!
read
exit
